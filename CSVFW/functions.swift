//
//  functions.swift
//  Framework Test
//
//  Created by David Keen on 12/04/2016.
//  Copyright © 2016 David Keen. All rights reserved.
//

import Foundation

/**File Name="file path"; read:"'true if reading, false if writing'"; value:"Content you want to write to"*/
public func readWriteToFile(fileName:String, read:Bool, value:String="") throws -> String
{
    let file = fileName //this is the file. we will write to and read from it
    
    let text = value //just a text
    
    if let dir : NSString = NSSearchPathForDirectoriesInDomains(NSSearchPathDirectory.DocumentDirectory, NSSearchPathDomainMask.AllDomainsMask, true).first {
        let path = dir.stringByAppendingPathComponent(file);
        
        if(read == true)
        {
            //reading
            do {
                return try NSString(contentsOfFile: path, encoding: NSUTF8StringEncoding) as String
            }
            catch {NSLog("Error reading")}
        } else {
            //writing
            do {
                try text.writeToFile(path, atomically: false, encoding: NSUTF8StringEncoding)
                return ""
            }
            catch {NSLog("Error writing")}
        }
    }
    return ""
}