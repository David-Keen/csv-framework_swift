# CSV-Framework-Swift
A Library for reading and writing to a CSV file


##Documentation:

Read or write to files:
readWriteToFile(fileName:String, read:Bool, value:String="") throws -> String

To read a file in your project you need to specify the file name/path and set the read argument to true as shown below.
readWriteToFile("data.csv", read:true)

If you want to write to the file, you again specify the file name/path except you set the read argument to false. You then need to say specify the document contents. E.g.:
readWriteToFile("date.csv", read:true, value:"Content to write")

##Using the CsvObject class

To create a CsvObject without specifying any variables (usefull when declaring variable types) you run the code below:

let data = CsvObject()


To initilise the data you need to recreate the variable using the arguments below.

CsvObject(fileName:String,headers:String,removeDupes:String="",dilimiter:String=",")

The required arguments that you need to parse in is the file name/path of the file and the headers (example below).

let data = CsvObject("data.csv", headers="column1,column2,column3...")

If your CSV file contains duplicate keys you can use the 'removeDupes' argument to delete keys that show up twice while the dilimiter key allows you to tell the object that your columns are not seperated by commas.

###CsvObject functions

####CsvObject().clear()
####CsvObject().writeRows([[String]])
####CsvObject().refresh()

To clear the CsvObject.csvInfo variable you need to run the clear() function. Note: This does not clear the csv file.

To write rows to the csv file you need to use the writeRows([[String]]) function.
Usage:
data.writeRows([["1","2","3"],["4","5","6"]])
This would append the text below to the file you set when initilising the variable:
1,2,3
4,5,6

Finaly, the refresh() function forces the CsvObject to update the csvInfo dictionary variable (usefull if file was written to indirectly from the object).


##Retreaving data from CsvObject.

To use the information that was in your information, you would use the CsvObject().csvInfo variable. For example:

File: 'data.csv'
1,2,3
4,5,6
7,8,9

Swift Code:

let data = CsvObject("data.csv", headers="column1,column2,column3")
print("\(data.csvInfo)") //Returns ["column1":["1","4","7"],"column2":["2","5","8"],"column3":["3","6","9"]]
print("\(data.csvInfo["column1"]!)") //Returns ["1","4","7"]