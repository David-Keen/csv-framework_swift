//
//  OSXCSVFW.h
//  OSXCSVFW
//
//  Created by David Keen on 18/04/2016.
//  Copyright © 2016 David Keen. All rights reserved.
//

#import <Cocoa/Cocoa.h>

//! Project version number for OSXCSVFW.
FOUNDATION_EXPORT double OSXCSVFWVersionNumber;

//! Project version string for OSXCSVFW.
FOUNDATION_EXPORT const unsigned char OSXCSVFWVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <OSXCSVFW/PublicHeader.h>


