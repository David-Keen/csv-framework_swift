//
//  CSVFW.swift
//  Framework Test
//
//  Created by David Keen on 12/04/2016.
//  Copyright © 2016 David Keen. All rights reserved.
//

import Foundation

/**Csv Object*/
public class CsvObject
{
    private var fileInformation = ""
    private var fileName = ""
    private var dilimiter = ""
    private var removeDupeKey = ""
    private var rows = [String]()
    private var headers = [String]()
    /**[String:[String]] = ["key":["row"]]*/
    public var csvInfo = [String:[String]]()
    
    
    /**Empty Initilizer*/
    public init(){}
    
    /**Required: file name, database headers. Optional: removeDupes (Used to clear multiple keys), dilimiter., headers="column1,column2..."*/
    public init(fileName:String,headers:String,removeDupes:String="",dilimiter:String=",")
    {
        self.fileName = fileName
        self.removeDupeKey = removeDupes
        do
        {
            self.fileInformation = try readWriteToFile(fileName, read:true)
        } catch {
            self.fileInformation = String()
        }
        self.dilimiter = dilimiter
        self.headers = headers.componentsSeparatedByString(",")
        self.refresh()
        
    }
    
    /**Clear the csvInfo dictionary, restores data on refresh*/
    public func clear()
    {
        self.csvInfo = [String:[String]]()
    }
    
    /**Refresh the csvInfo dictionary (usefull when the file was written to directly)*/
    public func refresh()
    {
        
        self.clear()
        self.fileInformation = try! readWriteToFile(self.fileName, read:true)
        rows = self.fileInformation.componentsSeparatedByString("\n")
        
        for header in self.headers
        {
            csvInfo[header] = [String]()
        }
        
        for row in rows
        {
            var index = 0
            let items = row.componentsSeparatedByString(self.dilimiter)
            
            for item in items
            {
                if items.count == self.headers.count
                {
                    self.csvInfo[self.headers[index]]?.append(item)
                }
                index += 1
            }
        }
    }
    
    
    /**Write list of rows ([[String]], each array is a row and the nested string array is the column data)*/
    public func writeRows(values:[[String]])
    {
        let colCount = values.count - 1
        let rowCount = values[0].count - 1
        
        for row in 0...rowCount
        {
            for column in 0...colCount
            {
                self.fileInformation += values[column][row]
                if(column < colCount){self.fileInformation += ","}
            }
            if(row == rowCount){self.fileInformation += "\n"}
        }
        try! readWriteToFile(self.fileName, read: false, value: self.fileInformation)
        self.refresh()
    }
    
}

let x = CsvObject(fileName: "data", headers: "ag")
